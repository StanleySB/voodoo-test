import React from 'react';
// style
import './page-loader.css'

const PageLoader = () => {
  return (
    <div className="gb-pageloader">
      <div className="lds-roller">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default PageLoader;
