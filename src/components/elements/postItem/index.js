import React from 'react'
// components
import { Card, CardContent, Typography } from '@material-ui/core'
// utils
import { getUserFromArrayById } from '../../../utils/getUserById'
// style
import "./post-item.css"

const PostItem = ({ post, users }) => {
  const user = getUserFromArrayById(post.userId, users)
  return (
    <Card variant="outlined" className="gb-post-item" >
      <CardContent>
        <Typography color="textSecondary">
          Post №{post.id}
        </Typography>
        <Typography component="h2">
          {post.title}
        </Typography>
        <Typography color="textSecondary">
          {post.body}
        </Typography>
        <Typography variant="body2" component="p">
          {user.name}
        </Typography>
      </CardContent>
    </Card >
  )
}

export default PostItem