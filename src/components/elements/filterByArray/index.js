import React from 'react'
// components
import { FormControl, InputLabel, Select, MenuItem, FormHelperText } from '@material-ui/core';


const FilterByArray = ({ array, filterByField, fieldToDisplay, filterTitle, filterHelperText, setFilter, filter }) => {
  const onChange = (event) => {
    setFilter(event.target.value)
  }

  return (
    <FormControl>
      <InputLabel>{filterTitle}</InputLabel>
      <Select
        labelId="demo-simple-select-helper-label"
        id="demo-simple-select-helper"
        value={filter}
        onChange={onChange}
      >
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        {array.map((item, id) => (
          <MenuItem
            value={item[filterByField]}
            key={id}>

            {item[fieldToDisplay]}
          </MenuItem>
        ))}
      </Select>
      <FormHelperText>{filterHelperText}</FormHelperText>
    </FormControl>
  )
}

export default FilterByArray