import React from 'react'
// components
import LayoutHeader from "../layoutHeader"
// style
import './layout-content.css'

const LayoutContent = (props) => {

  return (
    <section>
      <LayoutHeader />
      <main className="gb-main-content">{props.children}</main>
    </section>
  )
}

export default LayoutContent