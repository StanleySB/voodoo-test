import React from 'react'
// components
import { AppBar, Toolbar } from '@material-ui/core'


const LayoutHeader = (props) => {

  return (
    <AppBar position="static">
      <Toolbar>
        Header
      </Toolbar>
    </AppBar>
  )
}

export default LayoutHeader