import http from "../utils/http"

export const Posts = {

  list: (queryName, queryValue) => {
    // В идеале я бы отправлял userId=null, если не задан фильтр, но jsonplaceholder не позволяет отправить такой запрос
    // Расширять это не очень удобно, определенно есть путь лучше, но я пошел по простому
    if (!queryValue) {
      return http.get(`/posts`)
    } else {
      return http.get(`/posts`, {
        params: {
          [queryName]: queryValue
        }
      })
    }
  }
}