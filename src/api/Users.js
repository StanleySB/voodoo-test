import http from "../utils/http"

export const Users = {

  list: () => {
    return http.get(`/users`)
  }
}