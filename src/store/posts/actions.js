import { Posts } from "../../api/Posts";

export const types = {
  SEND: Symbol('SEND'),
  SEND_SUCCESS: Symbol('SEND_SUCCESS'),
  SEND_FAILURE: Symbol('SEND_FAILURE'),
};

export const postsActions = {
  fetch: (queryName, queryValue) => {
    return async dispatch => {
      dispatch({ type: types.SEND });
      try {
        const response = await Posts.list(queryName, queryValue);
        dispatch({ type: types.SEND_SUCCESS, payload: { posts: response.data } });
      } catch (e) {
        dispatch({ type: types.SEND_FAILURE, error: e.response.data });
        throw e;
      }
    };
  }
};
