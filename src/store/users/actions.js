import { Users } from "../../api/Users";

export const types = {
  SEND: Symbol('SEND'),
  SEND_SUCCESS: Symbol('SEND_SUCCESS'),
  SEND_FAILURE: Symbol('SEND_FAILURE'),
};

export const usersActions = {
  fetch: () => {
    return async dispatch => {
      dispatch({ type: types.SEND });
      try {
        const response = await Users.list();
        dispatch({ type: types.SEND_SUCCESS, payload: { users: response.data } });
      } catch (e) {
        dispatch({ type: types.SEND_FAILURE, error: e.response.data });
        throw e;
      }
    };
  }
};
