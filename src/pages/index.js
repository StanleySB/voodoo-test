import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux';
// actions
import { postsActions, usersActions } from '../store/actions';
// components
import LayoutContent from '../components/layouts/layoutContent';
import PageLoader from '../components/elements/pageLoader';
import FilterByArray from '../components/elements/filterByArray';
import PostItem from '../components/elements/postItem';


const App = (props) => {
  const [userFilter, setUserFilter] = useState('')
  const { posts, users, dispatch } = props

  useEffect(() => {
    dispatch(postsActions.fetch())
    dispatch(usersActions.fetch())
  }, [dispatch])

  useEffect(() => {
    dispatch(postsActions.fetch('userId', userFilter))
  }, [userFilter, dispatch])


  if (posts.isLoading || !posts.data || !users.data) {
    return (
      <LayoutContent>
        <PageLoader />
      </LayoutContent>
    )
  }

  return (
    <LayoutContent>
      <FilterByArray
        array={users.data}
        filterByField="id"
        fieldToDisplay="name"
        filterTitle='User'
        filterHelperText="Choose user"
        setFilter={setUserFilter}
        filter={userFilter} />
      <div className="gb-home__post-container">
        {posts?.data?.map((post, id) => (
          <PostItem post={post} users={users.data} key={id} />
        ))}
      </div>
    </LayoutContent >
  )
}

export default connect(state => ({
  users: state.users,
  posts: state.posts,
}))(App);
